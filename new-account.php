<?php 
include "includes/header.php";
session_start();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	handleSubmit($db);
}

$users = getUsers($db);
$accounts = getAllAccounts($db);
?>

<nav class="nav nav-tabs nav-justified">
  <?php 
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
      $nameTab = "My Account";
    }
      
  ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link " href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link active" href="logged-in.php"><?=$nameTab?></a>
  <?php
      if (isLoggedIn()) {
        echo '<a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>';
      }
  ?>
</nav>

<?php include "includes/form.php" ?>
<?php include "includes/footer.php" ?>
