<h1 id="pageHeader">Login</h1>
<form style="margin: auto; max-width: 600px;" action="login-submit.php" method="post">

  <div class="form-group">
    <input name="username" class="form-control" id="email" placeholder="Enter Userame" type="text" >
    <small style="padding-left: 12px;" id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>

  <div class="form-group" id="displayName" style="display: none;"> 
    <input name="display"  class="form-control"  placeholder="Enter First Name" type="text" >
    <small style="padding-left: 12px;" id="displayHelp" class="form-text text-muted">This is what we'll call you.</small>
  </div>

  <div class="form-group">
    <input name="password" class="form-control" id="pass" type="text" placeholder="Enter Password">
    <div style="padding: 2px;"></div>
    <input name="confirm" id="hiddenPass" type="text" style="display: none;" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
    <small style="padding-left: 12px;" id="passHelp" class="form-text text-muted">Your password will be encrpted for your safety.</small>
  </div>

  <div class="form-group">
    <button style="width: 100%;" id="loginButton" type="submit" class="btn btn-primary"><div id="LoginButtonText">Login</div></button>
  </div>

  <div class="form-group">
    <button style="width: 100%;" type="button" id="createButton" class="btn btn-outline-primary"><div id="createButtonText">Create an Account</div></button>
  </div>

</form>

<script>
$(document).ready(function(){ //This toggles between login and create account 
  $("#createButton").click(function(){
    if ($("#hiddenPass").css("display") == "block") {
      $("#hiddenPass").slideUp("fast");
      $('#createButtonText').fadeOut('fast', function() {
        $(this).html("Create Account").fadeIn('fast');
      }); 
      $('#LoginButtonText').fadeOut('fast', function() {
        $(this).html("Login").fadeIn('fast');
      }); 

      $('#pageHeader').fadeOut('fast', function() {
        $(this).html("Login").fadeIn('fast');
      }); 
      $(".form-check").css("display", "block"); 
      $("#displayName").css("display", "none");     
        
    }

    else {
      $("#hiddenPass").slideDown("fast");
      $('#createButtonText').fadeOut('fast', function() {
        $(this).html("Nevermind, Back to the login").fadeIn('fast');
      }); 
      $('#LoginButtonText').fadeOut('fast', function() {
        $(this).html("Create My Account!").fadeIn('fast');
      }); 
      $('#pageHeader').fadeOut('fast', function() {
        $(this).html("Account Creation").fadeIn('fast');
      }); 
      $(".form-check").css("display", "none");
      $("#displayName").css("display", "none");
    }

  });
});
</script>