<?php

function getResult(string $sql, mysqli $conn) : Traversable
{
	$result = $conn->query($sql);

	if (!$result) {
		echo $conn->error;
		return [];
	}

	return $result;
}

//returns all users
function getUsers(mysqli $conn) : Traversable {
    $sql = 'SELECT * FROM users;';
    return getResult($sql, $conn);
}

//returns all accounts
function getAllAccounts(mysqli $conn) : Traversable {
	$uname = strtolower($_SESSION['username']);
	if ($uname == "admin") {
		$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id";
		return getResult($sql, $conn);	
	}
	else {
		$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE username = '$uname'";
		return getResult($sql, $conn);		
	}

}

//returns the user account with corresponding username input
function getUserAccount(mysqli $conn, $username) : Traversable {
	$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE username = '$username'";
	return getResult($sql, $conn);
}

//returns all osrs accounts and checks for sort data in GET
function getOSRSAccounts(mysqli $conn) : Traversable {
	if(isset($_GET['sort'])) {
		$sort = $_GET['sort'];
	
		if($sort == "accounts.game_username" || $sort == "users.username") { // if account name or username are selected, sorted in ascending order
			$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE accounts.game_name = 'OSRS' ORDER BY $sort ASC;";
			return getResult($sql, $conn);			
		}
		else { // all other fields are sorted in descending order
			$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE accounts.game_name = 'OSRS' ORDER BY $sort DESC;";
			return getResult($sql, $conn);			
		}
	}

	else {
		$sql = 'SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE accounts.game_name = "OSRS" ORDER BY accounts.game_username ASC';
		return getResult($sql, $conn);		
	}

}

//returns all rs3 accounts and checks for sort data in GET
function getRS3Accounts(mysqli $conn) : Traversable {
	if(isset($_GET['sort'])) {
		$sort = $_GET['sort'];
		
		if($sort == "accounts.game_username" || $sort == "users.username") { // if account name or username are selected, sorted in ascending order
			$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE accounts.game_name = 'RS3' ORDER BY $sort ASC;";
			return getResult($sql, $conn);			
		}
		else { // all other fields are sorted in descending order
			$sql = "SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE accounts.game_name = 'RS3' ORDER BY $sort DESC;";
			return getResult($sql, $conn);			
		}
	}

	else {
		$sql = 'SELECT * FROM users INNER JOIN accounts ON accounts.user_id = users.id WHERE accounts.game_name = "RS3" ORDER BY accounts.game_username ASC';
		return getResult($sql, $conn);		
	}
}

//returns bio for the given username
function getBio(mysqli $db, $username) : string {
	$query = "SELECT bio FROM users WHERE username = '$username'";
	$result = $db->query($query);
	$bioObject = $result->fetch_object();
	$bio = $bioObject->bio;
	return $bio;
}

//submits data from the new game form
function handleSubmit(mysqli $conn) : void {
	$id = $_POST['id'];
	$user = $_SESSION['user_id'];
	$gameName = $_POST['game_name'];
	$gameUsername = $_POST['game_username'];
	$combatLevel = $_POST['combat_level'];
	$totalLevel = $_POST['total_level'];
	$questPoints = $_POST['quest_points'];
	$totalWealth = $_POST['total_wealth'];
	
	// if not valid throw error on screen
	if (!isValid($gameUsername, $combatLevel, $totalLevel, $questPoints, $totalWealth)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		return;
	}
	//check to make sure combat level is within game limits
	if ($gameName == "RS3" && ($combatLevel > 138 || $combatLevel < 3)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your combat level is between 3 and 138. Thanks."); });',	'</script>';

		return;
	}
	//check to make sure combat level is within game limits
	if ($gameName == "OSRS" && ($combatLevel > 126 || $combatLevel < 3)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your combat level is between 3 and 126. Thanks."); });',	'</script>';
		return;
	}
	//check to make sure total level is within game limits
	if ($gameName == "RS3" && ($totalLevel > 2736 || $totalLevel < 28)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your total level is between 28 and 2736. Thanks."); });',	'</script>';
		return;
	}
	//check to make sure total level is within game limits
	if ($gameName == "OSRS" && ($totalLevel > 2277 || $totalLevel < 28)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your total level is between 28 and 2277. Thanks."); });',	'</script>';
		return;
	}
	//check to make sure quest points are within game limits
	if ($gameName == "RS3" && ($questPoints > 410 || $questPoints < 0)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your quest points are between 0 and 410. Thanks."); });',	'</script>';
		return;
	}
	//check to make sure quest points are within game limits
	if ($gameName == "OSRS" && ($questPoints > 266 || $questPoints < 0)) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your quest points are between 0 and 266. Thanks."); });',	'</script>';
		return;
	}
	//check to make sure account wealth isn't less than 0
	if ($totalWealth < 0) {
		echo	'<script>', '$(document).ready(function(){ $("#formErrorMessage").css("display","block"); });',	'</script>';
		echo	'<script>', '$(document).ready(function(){ $("#errorMessageData").html("Please make sure your account wealth is not negative. Thanks."); });',	'</script>';
		return;
	}
	// Everything checks out, begin inserting/updating data

	// if form id is present, update the existing record
	if (is_numeric($id)) {
		$sql = "UPDATE accounts SET game_name='$gameName', game_username='$gameUsername', combat_level=$combatLevel, total_level=$totalLevel, quest_points=$questPoints, total_wealth=$totalWealth   WHERE rs_account_id =$id;";
	} 
	
	//if no form id, create new account
	else { 
		echo("DATA: "); echo($user); echo($_SESSION['user_id']);
		$sql = "INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) VALUES ('$user', '$gameName', '$gameUsername', '$combatLevel', '$totalLevel', '$questPoints', '$totalWealth');";
	}
	

	$result = $conn->query($sql);

	if (!$result) {
		echo "<p>Error: $conn->error</p>";
		echo print_r($_SESSION);
		echo "<p><br></p>";
		echo print_r($_POST);

		return;
	}
	
	header('Location: logged-in.php');
}

// check to make sure the data types are acceptable
function isValid(string $gameUsername, string $combatLevel, string $totalLevel, string $questPoints, string $totalWealth) : bool
{
	return !empty($gameUsername) && is_numeric($combatLevel) && is_numeric($totalLevel) && is_numeric($questPoints) && is_numeric($totalWealth);
}

//user login
function login(mysqli $db) : void {
	$username = $_POST['username'];
	$pass = $_POST['password'];

	// error for not filling in all fields 
	if (empty($username) || empty($pass)) {
		include 'includes/errors/allFieldsRequired.php';
		return;
	}

	// running query
	$query = "SELECT * FROM users WHERE username = '$username'";
	$result = $db->query($query);

	// echo any problems
	if (!$result) {
		echo $db->error;
		return;
	}

	// error for cant find user
	if ($result->num_rows <= 0) {
		include 'includes/errors/cantFindUser.php';
		return;
	}

	// user found
	$user = $result->fetch_object();

	//error for incorrect password
	if (!password_verify($pass, $user->pass_hash)) {
		include 'includes/errors/incorrectPassword.php';
		return;
	}

	// Now logged in
	$_SESSION['username'] = $username;

	//update session
	$query = "SELECT id FROM users WHERE username = '$username'";
	$result = $db->query($query);
	$id = $result->fetch_object();
	$_SESSION['user_id'] = $id->id;

	header('Location: logged-in.php');
}

//terminates the session, logs user out
function logout() : void {
	$_SESSION = [];
	header('Location: index.php');
}

//checks to see if user is logged in via session
function isLoggedIn() : bool {
	return isset($_SESSION['username']);
}

//creates new user in db
function createUser(mysqli $db) : void {
	$user = $_POST['username'];
	$pass = $_POST['password'];
	$conf = $_POST['confirm'];

	//throws error if fields aren't filled in
	if (empty($user) || empty($pass) || empty($conf)) {
		include 'includes/errors/allFieldsRequired.php';
		return;
	}

	//throws error if password and the password confirmation dont match
	if ($pass !== $conf) {
		include 'includes/errors/passwordsDontMatch.php';
		return;
	}
	
	$hash = password_hash($pass, PASSWORD_DEFAULT);
	$query = "INSERT users (username, pass_hash, bio) VALUES ('$user', '$hash', 'Hi, I am new here!')";
	$result = $db->query($query);

	if (!$result) {
		echo $db->error;
		echo($result);
		return;
	}

	$_SESSION['username'] = $user;

	$query = "SELECT id FROM users WHERE username = '$user'";
	$result = $db->query($query);
	$id = $result->fetch_object();
	$_SESSION['user_id'] = $id->id;

	header('Location: logged-in.php');
}

//updates password
function updateUser(mysqli $db) : void {
	$user = $_SESSION['username'];
	$pass = $_POST['password'];
	$conf = $_POST['confirm'];

	$_SESSION['username'] = $user;
	$query = "SELECT id FROM users WHERE username = '$user'";
	$result = $db->query($query);
	$id = $result->fetch_object();
	$_SESSION['user_id'] = $id->id;
	$userID = $_SESSION['user_id'];

	//throws error if fields arent all filled in
	if (empty($user) || empty($pass) || empty($conf)) {
		include 'includes/errors/allFieldsRequired.php';
		return;
	}

	//throws error if passwords dont match
	if ($pass !== $conf) {
		include 'includes/errors/passwordsDontMatch.php';
		return;
	}
	
	$hash = password_hash($pass, PASSWORD_DEFAULT);
	$query = "UPDATE users SET pass_hash = '$hash' WHERE id = '$userID';";

	$result = $db->query($query);

	if (!$result) {
		echo $db->error;
		echo($result);
		return;
	}
	
	header('Location: logged-in.php');
}

//updates the users bio based on the user ID passed in from POST
function updateBio(mysqli $db) : void {
	$user = $_SESSION['username'];
	$bio = $_POST['bio'];

	$_SESSION['username'] = $user;
	$query = "SELECT id FROM users WHERE username = '$user'";
	$result = $db->query($query);
	$id = $result->fetch_object();
	$_SESSION['user_id'] = $id->id;
	$userID = $_SESSION['user_id'];

	$query = $db->prepare("UPDATE users SET bio = ? WHERE id = ?;");
	$query->bind_param('si', $bio, $userID);

	if (!$query->execute()) {
		echo $query->error;
		return;
	}
	
	header('Location: logged-in.php');
}

//Uploads image
function handleUpload() : void {
	$db = new mysqli('localhost','root','','paulpresslerdb');

	if (isset($_FILES['upload'])) {
		$file = $_FILES['upload'];
		$filename = $file['name'];
		$size = $file['size'];
		$type = $file['type'];
		$tmpPath = $file['tmp_name'];		
	};

	//Error uploading file
	if (!isset($_FILES['upload'])) {
		echo("Error Uploading FIle");
		return;
	};

	//Error no file selected
	if ($filename == "") {
		echo("No File Was Selected");
		return;
	}	

	//finds user ID to insert image into db
	$user = $_SESSION['username'];
	$query = "SELECT id FROM users WHERE username = '$user'";
	$result = $db->query($query);
	$id = $result->fetch_object();
	$userID = $id->id;

	$allowedTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif',];

	$finfo = new finfo(FILEINFO_MIME_TYPE);
	
	//Error bad file type
	if (!in_array($finfo->file($tmpPath), $allowedTypes)) {
		echo '<p>File type not allowed! Type: ' . $finfo->file($tmpPath) . '</p>';
		return;
	}

	$stream = fopen($tmpPath, 'r');
	$data = fread($stream, $size);
	fclose($stream);

	//updates the db with image data
	$query = $db->prepare("UPDATE users SET image_data=(?), image_filename=(?), image_type=(?) WHERE id = '$userID';");
	$query->bind_param('sss', $data, $filename, $type);

	if (!$query->execute()) {
		echo $query->error;
		return;
	}

	//Confirms the image upload worked
	echo("
		<script type='text/javascript'>
			function pageRedirect() {
				window.location.replace('edit-login.php');
			}      
			setTimeout('pageRedirect()', 1000);
		</script>
		<h2>Successful Upload!</h2>
		<h2>Automatically Redirecting In 1 Second...</h2>
	");
}
