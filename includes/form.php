<h1>Player Information</h1>

<form action="" method="post">
		<input type="hidden" name="id"
			value="<?=isset($account) ? $account->rs_account_id : ''?>" />
		<div class="form-group row  offset-lg-3">
			<label class="col-sm-3 col-form-label" for="game_name">Game Version</label>
			<div class="col-lg-5">
					<select class="form-control" id="game_name" name="game_name">
						<?php $selectGameVersion = $account->game_name;?>
						<option></option>
						<option <?php if ($selectGameVersion == 'OSRS'){echo('selected ');}?>value="OSRS">Old School</option>
						<option <?php if ($selectGameVersion == 'RS3'){echo('selected ');}?>value="RS3">RuneScape 3</option>
					</select>
			</div>
		</div>
		
		<div class="form-group row  offset-lg-3">
			<label class="col-sm-3 col-form-label" for="game_username">In-Game Username</label>
			<div class="col-lg-5">
					<input class="form-control" id="game_username" name="game_username" type="text"
						value="<?=isset($account) ? $account->game_username : '' ?>"/>
			</div>
		</div>

		<div class="form-group row  offset-lg-3">
			<label class="col-sm-3 col-form-label" for="combat_level">Combat Level</label>
				<div class="col-lg-5">
					<input class="form-control" id="combat_level" name="combat_level" type="text"
						value="<?=isset($account) ? $account->combat_level : '' ?>"/>
				</div>
		</div>

		<div class="form-group row  offset-lg-3">
			<label class="col-sm-3 col-form-label" for="total_level">Total Level</label>
				<div class="col-lg-5">
					<input class="form-control" id="total_level" name="total_level" type="text"
						value="<?=isset($account) ? $account->total_level : '' ?>"/>
				</div>
		</div>

		<div class="form-group row  offset-lg-3">
			<label class="col-sm-3 col-form-label" for="quest_points">Quest Points</label>
				<div class="col-lg-5">
					<input class="form-control" id="quest_points" name="quest_points" type="text"
						value="<?=isset($account) ? $account->quest_points : '' ?>"/>
				</div>
		</div>

		<div class="form-group row  offset-lg-3">
			<label class="col-sm-3 col-form-label" for="total_wealth">Total Wealth (M)</label>
				<div class="col-lg-5">
					<input class="form-control" id="total_wealth" name="total_wealth" type="text"
						value="<?=isset($account) ? $account->total_wealth : '' ?>"/>
				</div>
		</div>
		
		<div class="form-group row offset-lg-3">
			<button style="" class="btn btn-success col-lg-8" type="submit">Save Character Information</button>
		</div>

		<div style="display: none; width: 60%; margin: auto;" id="formErrorMessage" class="alert alert-danger alert-dismissible fade show" role="alert">
			<h4 class="alert-heading">Oops, that's not right...</h4>
			<p>Having trouble? Make sure each field has the correct data entered and try again.</p>
			<hr>
			<p id="errorMessageData">Choose a game version<br>Enter your in-game username<br>Enter your combat level, between 3 and 126,138<br>Enter a total level<br>Enter quest points<br>Enter total account value in coins</p>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
</form>