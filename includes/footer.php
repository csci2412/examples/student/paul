</div> <!-- This closes the container class that starts in includes/header.php-->
<?php 
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
        $nameTab = "My Account";
    }    
?>

<div id="footer" class="fixed-bottom">
    <footer>&copy;Friends Hiscores - 2018 - ppressler@student.cscc.edu <br>
    <a href="index.php">Home Page</a> | 
    <a href="osrs.php">Old School RuneScape</a> | 
    <a href="rs3.php">RuneScape 3</a> | 
    <a href="logged-in.php"><?=$nameTab?></a> 
    <?php
        if (isLoggedIn()) {
            echo ' | <a href="edit-login.php">Settings</a>';
            echo ' | <a href="logout.php">Logout</a>';
        }
    ?>
    </footer>
</div>

</body>
</html>
