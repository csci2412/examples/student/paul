<?php 
include "includes/header.php";

$users = getUsers($db);
$account = getOSRSAccounts($db);
?>

<nav class="nav nav-tabs nav-justified">
  <?php 
    session_start();
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
      $nameTab = "My Account";
    }
      
  ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link" href="logged-in.php"><?=$nameTab?></a>
  <?php
      if (isLoggedIn()) {
        echo '<a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>';
      }
  ?>
</nav>

<h1 id="pageHeader">Account Settings</h1>


<form action="upload.php" method="post" enctype="multipart/form-data" style="margin: auto; max-width: 600px; padding-bottom: 30px;">
  <div class="form-group">
    <img src="get-image.php" class="centered" id="profile-pic"/>
    <label style="font-size: 18px; font-weight: bold; float: left; " for="input">Profile Picture</label>
    <div class="input-group mb-3">
      <div class="custom-file">
        <input type="file" name="upload" class="custom-file-input" id="upload" onchange="$('#upload-file-info').html('Selected file: ' + (this.value).replace(/.*[\/\\]/, ''));">
        <label id="upload-file-info" class="custom-file-label" for="input">Choose file</label>
      </div>
      <div class="input-group-append">
        <button class="btn btn-primary" type="submit">Upload</button>
      </div>
    </div>
  </div>
</form>

<form style="margin: auto; max-width: 600px; padding-bottom: 30px;" action="edit-login-submit.php" method="post">
  <div class="form-group">
    <label style="font-size: 18px; font-weight: bold;" for="pass">Change Password</label>
    <input name="password" class="form-control" id="pass" type="text" placeholder="Enter New Password">
    <div style="padding: 2px;"></div>
    <input name="confirm" class="form-control" id="confirm" type="text" placeholder="Confirm New Password">
    <div style="padding-top: 4px;" class="form-group">
      <button style="width: 100%;" id="loginButton" type="submit" class="btn btn-primary">Update Password</button>
    </div>
  </div>
</form>

<form style="margin: auto; max-width: 600px;" action="edit-bio.php" method="post">
  <div class="form-group">
    <label style="font-size: 18px; font-weight: bold;" for="pass">Update Bio</label>
    <textarea name="bio" placeholder="Bio goes here..." class="form-control" id="bio" rows="3"><?=getBio($db, $_SESSION['username']);?></textarea>
    <div style="padding-top: 4px;" class="form-group">
      <button style="width: 100%;" id="updateBioButton" type="submit" class="btn btn-primary">Update</button>
    </div>
  </div>
</form>

<div style="padding-top: 40px; margin: auto; max-width: 600px;" class="form-group">
  <button style="width: 100%;" type="button" onclick="location.href='logged-in.php';" class="btn btn-dark">Go Back</button>
</div>


<?php include "includes/footer.php" ?>