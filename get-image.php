<?php
session_start();
include 'includes/mysql-connect.php';
include 'includes/functions.php';

if(isset($_GET['id'])) {
    $newUserID = $_GET['id'];
    $db = new mysqli('localhost','root','','paulpresslerdb');
    $stmt = $db->prepare('select * from users where id = ?');
    $stmt->bind_param('i', $newUserID);

    if (!$stmt->execute()) exit();
    
    $result = $stmt->get_result();
    $img = $result->fetch_object(); 
}

else {
    $user = $_SESSION['username'];
    $query = "SELECT id FROM users WHERE username = '$user'";
    $result = $db->query($query);
    $id = $result->fetch_object();
    $userID = $id->id;   

    $db = new mysqli('localhost','root','','paulpresslerdb');
    $stmt = $db->prepare('select * from users where id = ?');
    $stmt->bind_param('i', $userID);

    if (!$stmt->execute()) exit();

    $result = $stmt->get_result();
    $img = $result->fetch_object();    
}


if ($img->image_type == null) {
    $image = file_get_contents("assets/images/default.png");
    header('Content-type: image/png;');
    header("Content-Length: " . strlen($image));
    echo $image;
}


else {
    header('Content-Type: ' . $img->image_type);
    echo $img->image_data;
}