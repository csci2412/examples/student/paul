drop database if exists paulpresslerdb;
create database if not exists paulpresslerdb;
use paulpresslerdb;

create table users(
	id int unsigned auto_increment primary key,
	username nvarchar(255) not null,
    pass_hash char(60),
	image_filename nvarchar(255),
    image_type nvarchar(50),
    image_data longblob,
    bio nvarchar(255),
    unique(username)
);

create table accounts(
	rs_account_id int unsigned not null auto_increment primary key,
    user_id int unsigned not null,
    game_name nvarchar(20) not null,
    game_username nvarchar(20) not null,
    combat_level int(3) not null,
    total_level int(4) not null,
    quest_points int(3) not null,
    total_wealth int(5) not null,
    foreign key (user_id) references users(id)
);
INSERT INTO users (username, pass_hash, bio) values ('Admin', '$2y$10$syVHR/AsLTfaCbtXHKNAZuEC0d9eNH35FP9NcOdAZxcYBK0yU5h7m', "This account can edit or delete any other game accounts."); #MUST HAVE ADMIN, DON'T DELETE - USERNAME = 'admin' - PASSWORD = 'password'


#Some sample data so you can see the tables
INSERT INTO users (username, pass_hash, bio) values ('Paul', '$2y$10$Z/LuqgdhNLZH394.nTTWQuABKOV.NyCUc3dEsawlX8ugd1vA8RsyW', "Hi, My name is Paul Pressler and I'm a student at columbus state.");
INSERT INTO users (username, pass_hash, bio) values ('Brandon', '$2y$10$Z/LuqgdhNLZH394.nTTWQuABKOV.NyCUc3dEsawlX8ugd1vA8RsyW', "Hi, My name is Brandon Bumpus. I only play games after 8pm. I'll see you in the game!");
INSERT INTO users (username, pass_hash, bio) values ('Ryan', '$2y$10$Z/LuqgdhNLZH394.nTTWQuABKOV.NyCUc3dEsawlX8ugd1vA8RsyW', "What's up? I'm ryan daniels. add me on instagram @ryanrunescape");
INSERT INTO users (username, pass_hash, bio) values ('Justin', '$2y$10$Z/LuqgdhNLZH394.nTTWQuABKOV.NyCUc3dEsawlX8ugd1vA8RsyW', "Bios arent cool... so i dont have one! :)");
INSERT INTO users (username, pass_hash, bio) values ('Jessie', '$2y$10$Z/LuqgdhNLZH394.nTTWQuABKOV.NyCUc3dEsawlX8ugd1vA8RsyW', '');

INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (2, 'RS3', 'Paulby', 138, 2485, 314, 268);
INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (3, 'RS3', 'Yhutto', 138, 2722, 278, 166);
INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (4, 'RS3', 'Danrian', 112, 1908, 289, 821);
INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (5, 'RS3', 'Jarl Ivar', 135, 2530, 301, 644);

INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (2, 'OSRS', 'U-Haul Paul', 86, 1192, 130, 21);
INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (3, 'OSRS', '99DefNub', 125, 1903, 232, 128);
INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (4, 'OSRS', 'Danrian', 77, 1172, 251, 221);
INSERT INTO accounts (user_id, game_name, game_username, combat_level, total_level, quest_points, total_wealth) values (6, 'OSRS', 'Belenardo', 122, 1807, 266, 108);
