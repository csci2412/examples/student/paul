<?php 
include "includes/header.php";
$users = getUsers($db);
$account = getOSRSAccounts($db);
?>

<nav class="nav nav-tabs nav-justified">
  <?php 
    session_start();
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
      $nameTab = "My Account";
    }
      
  ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link active" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link" href="logged-in.php"><?=$nameTab?></a>
  <?php
      if (isLoggedIn()) {
        echo '<a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>';
      }
  ?>
</nav>
  <h1>Old School RuneScape HiScores</h1>

  <img src="assets/images/osrsHeader.png" style="width: 100%;">

  <div id="hidden">
    <table class="table table-hover table-bordered table-striped table-dark">
      <thead class="thead-dark">
        <tr>
          <th></th>
          <th><a id="tableLink" href="osrs.php?sort=users.username">Acc Owner</a></th>
          <th><a id="tableLink" href="osrs.php?sort=accounts.game_username">Username</a></th>            
          <th><a id="tableLink" href="osrs.php?sort=accounts.combat_level">Combat</a></th>
          <th><a id="tableLink" href="osrs.php?sort=accounts.total_level">Total</a></th>
          <th><a id="tableLink" href="osrs.php?sort=accounts.quest_points">Quest's</a></th>
          <th><a id="tableLink" href="osrs.php?sort=accounts.total_wealth">Total GP</a></th>
          <th>Tools</th>
        </tr>
      </thead>

      <?php $count = 0; ?>
      <?php foreach ($account as $accounts): ?> 
      <tr>
        <?php $count++; ?>
        <td><?= $count ?></td>
        <td>
          <div class="imagetable">
            <div> <img style="border-radius: 50%;" src="get-image.php?id=<?=$accounts['id']?>"> </div>
            <div><a id="tableLink" href="userAccount.php?username=<?=$accounts['username']?>&id=<?=$accounts['id']?>"><?=$accounts['username']?></a></div>
          </div>
        </td>

        <td>
          <div class="imagetable">
            <div> <img src="assets/images/osrsName.png"> </div>
            <div><?=$accounts['game_username']?></div>
          </div>
        </td>

        <td>
          <div class="imagetable">
            <div> <img src="assets/images/osrsCombat.png"> </div>
            <div><?=$accounts['combat_level']?></div>
          </div>
        </td>
        
        <td>
          <div class="imagetable">
            <div> <img src="assets/images/osrsTotal.png"> </div>
            <div><?=$accounts['total_level']?></div>
          </div>
        </td>
        
        <td>
          <div class="imagetable">
            <div> <img src="assets/images/osrsQuest.png"> </div>
            <div><?=$accounts['quest_points']?></div>
          </div>
        </td>
        
        <td>
          <div class="imagetable">
            <div> <img src="assets/images/osrsCoins.png"> </div>
            <div style="color: #00e600;"><?=$accounts['total_wealth']?>M</div>
          </div>
        </td>
        
        <td>
          <?php $link = $accounts['game_username']; ?>
          <button onclick="location.href='https://secure.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=<?php echo($link)?>'" type="button" class="btn btn-secondary btn-sm">HiScore</button>		
        </td>
      
      </tr>
      <?php endforeach; ?>
    </table>
  </div>

<script>
  $(document).ready(function(){ //makes table slide down on page load
      $('#hidden').slideDown(700);
  });

  $(document).on("click", "#tableLink", function () { //makes table slide up when a table header link is clicked
    var newUrl = $(this).attr("href");
    $("#hidden").slideUp(150, function () {
        location = newUrl;
    });

    return false;// prevent the default browser behavior.
  });
</script>

<?php include "includes/footer.php" ?>