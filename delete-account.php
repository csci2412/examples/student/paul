<?php
include 'includes/mysql-connect.php';

$id = $_GET['id'] ?? null;

if (!$id) {
	exit();
}

$sql = "DELETE FROM accounts WHERE rs_account_id = $id";
$result = $db->query($sql);

if (!$result) {
	echo $db->error;
	exit();
}

header('Location: logged-in.php');
