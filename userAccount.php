<?php 
include "includes/header.php";
$username = $_GET['username'];
$id = $_GET['id'];
$users = getUsers($db);
$account = getUserAccount($db, $username);
?>

<nav class="nav nav-tabs nav-justified">
  <?php 
    session_start();
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
      $nameTab = "My Account";
    }
      
  ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link" href="logged-in.php"><?=$nameTab?></a>
  <?php
      if (isLoggedIn()) {
        echo '<a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>';
      }
  ?>
</nav>
<div style="padding-bottom: 20px;">
  <h1><img id="profile-pic" src="get-image.php?id=<?=$_GET['id']?>"> &nbsp&nbsp&nbsp&nbspWelcome To <?=ucfirst($_GET['username'])?>`s Page!</h1>
  <div class="card">
    <div class="card-header">
      <h3 style="text-align: left;">Bio</h3>
    </div>
    <div class="card-body">
      <blockquote class="blockquote mb-0">
        <p><?=getBio($db, $username);?></p>
      </blockquote>
    </div>
  </div>
</div>
<div style="width: 100%;">
  <h2 style="float: left;">Their RuneScape Accounts</h2>
</div>

<table class="table table-hover table-bordered table-striped table-dark">
	<thead class="thead-dark">
    <tr>
        <th>Game Version</th>
        <th>Username</th>            
        <th>Combat Level</th>
        <th>Total Level</th>
        <th>Quest Points</th>
        <th>Total Wealth</th>
    </tr>
  </thead>

  <?php foreach ($account as $accounts): ?>
  <tr>
    <?php $gameVersion = $accounts['game_name'];
          if ($gameVersion == 'OSRS'){
            $gameVersion = 'Old School';
          }
          else {
            $gameVersion = 'Runescape 3';
          }
    ?>
    <td><?=$gameVersion?></td>
    <td><?=$accounts['game_username']?></td>
    <td><?=$accounts['combat_level']?></td>
    <td><?=$accounts['total_level']?></td>
    <td><?=$accounts['quest_points']?></td>
    <td style="color: #00e600;"><?=$accounts['total_wealth']?>M</td>
  </tr>
  <?php endforeach; ?>

</table>

  <div class="form-group" style="width: 500px; margin: auto;" >
    <button onclick="goBack()" style="width: 100%;" id="backButton" type="button" class="btn btn-primary">Go Back</button>
  </div>

<script>
  function goBack() {
      window.history.back();
  }
</script> 
<?php include "includes/footer.php"?>