<?php 
include "includes/header.php";
session_start();
if (!isLoggedIn()) {
	header('Location: login.php');
}

$users = getUsers($db);
$account = getAllAccounts($db);
?>

<nav class="nav nav-tabs nav-justified">
    <?php 
      $display = "none";
      if ($_SESSION['username'] == "") {
        $nameTab = "Login";
      }
        
      else {
        $nameTab = "My Account";
      }
        
    ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link active" href="logged-in.php"><?=$nameTab?></a>
  <a class="nav-item nav-link" id="logout" style="display: <?$display?>;" href="logout.php">Logout</a>

    <?php
      if (isLoggedIn()) {
        $display = "block";
      }
    ?>
</nav>
<div style="padding-bottom: 20px;">

<h1><img id="profile-pic" src="get-image.php"/>&nbsp&nbsp&nbsp&nbspWelcome <?=ucfirst($_SESSION['username'])?>, You're Logged In! </h1>
<div class="card">
  <div class="card-header">
    <h3 style="text-align: left;">Bio</h3>
  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p><?=getBio($db, $_SESSION['username']);?></p>
    </blockquote>
  </div>
</div>
</div>
<div style="width: 100%;">
  <h2 style="float: left;">My RuneScape Accounts</h2>
  <button style="float: right;" onclick="location.href='edit-login.php';" style="width: 100%;" id="loginButton" type="button" class="btn btn-dark">Account Settings</button>
</div>

<table class="table table-hover table-bordered table-striped table-dark">
	<thead class="thead-dark">
    <tr>
        <th>Game Version</th>
        <th>Username</th>            
        <th>Combat Level</th>
        <th>Total Level</th>
        <th>Quest Points</th>
        <th>Total Wealth</th>
        <th>Tools</th>
    </tr>
  </thead>

  <?php foreach ($account as $accounts): ?>
  <tr>
    <?php $gameVersion = $accounts['game_name'];
          if ($gameVersion == 'OSRS'){
            $gameVersion = 'Old School';
          }
          else {
            $gameVersion = 'Runescape 3';
          }
    ?>
    <td><?=$gameVersion?></td>
    <td><?=$accounts['game_username']?></td>
    <td><?=$accounts['combat_level']?></td>
    <td><?=$accounts['total_level']?></td>
    <td><?=$accounts['quest_points']?></td>
    <td style="color: #00e600;"><?=$accounts['total_wealth']?>M</td>
    <td style="padding-left: 0px; padding-right: 0px;">
        <button onclick="location.href='edit-account.php?id=<?=$accounts['rs_account_id']?>';" type="button" class="btn btn-warning btn-sm">&nbsp&nbspEdit&nbsp&nbsp</button>
        <button onclick="location.href='delete-account.php?id=<?=$accounts['rs_account_id']?>';" type="button" class="btn btn-danger btn-sm">Delete</button>			
    </td>
  </tr>
  <?php endforeach; ?>

</table>

  <div class="form-group" style="width: 500px; margin: auto;" >
    <button onclick="location.href='new-account.php';" style="width: 100%;" id="loginButton" type="button" class="btn btn-success">Add RuneScape Account</button>
  </div>

<?php include "includes/footer.php"?>