<?php
    session_start();
    include "includes/header.php";
?>

<nav class="nav nav-tabs nav-justified">
  <?php
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
      $nameTab = "My Account";
    }
      
  ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link" href="logged-in.php"><?=$nameTab?></a>
  <?php
      if (isLoggedIn()) {
        echo '<a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>';
      }
  ?>
</nav>

<h1 style="padding-top: 50px;">Image Upload Status</h1>

<h2>
    <?php     
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            handleUpload($db);
        } 
    ?>
</h2>

<div style="width: 20%; margin: auto;">
    <button onclick="location.href='edit-login.php'" style="width: 100%;" class="btn btn-primary" type="button">Go Back</button>
</div>

<?php include "includes/footer.php";