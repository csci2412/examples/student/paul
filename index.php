<?php 
include "includes/header.php";
?>

<nav class="nav nav-tabs nav-justified">
  <?php 
    session_start();
    $display = "none";
    $nameTab = "Login";
    if (isset($_SESSION['username'])) {
      $nameTab = "My Account";
    }
      
  ?>
  <a class="nav-item nav-link active" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link" href="logged-in.php"><?=$nameTab?></a>
  <?php
      if (isLoggedIn()) {
        echo '<a class="nav-item nav-link" id="logout" href="logout.php">Logout</a>';
      }
  ?>
</nav>

<h1 style="padding-bottom: 20px;">Friend's HiScores</h1>

<h2 style="padding-bottom: 20px;">Keep Track of your friend's HiScores.  </h2>

<h4>
  Friend's HiScores is a great new tool to help keep track of your progress 
  and put it up against your friends, your clan, and maybe even your rivals!
  Join Friend's HiScores today and findout where you stack up against the rest. 
</h4>

<img src="assets/images/rsArt.png" style="width: 100%;">

<?php include "includes/footer.php";?>