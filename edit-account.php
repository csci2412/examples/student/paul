<?php
include "includes/header.php";
session_start();
$users = getUsers($db);
$account = getAllAccounts($db);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	handleSubmit($db);
}

if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$result = getResult("SELECT * FROM accounts WHERE rs_account_id = $id", $db);

	if ($result) {
		$account = $result->fetch_object();
	}
}
?>

<nav class="nav nav-tabs nav-justified">
    <?php 
      $display = "none";
      if ($_SESSION['username'] == "") {
        $nameTab = "Login";
      }
        
      else {
        $nameTab = "My Account";
      }
        
    ?>
  <a class="nav-item nav-link" href="index.php">Home Page</a>
  <a class="nav-item nav-link" href="osrs.php">Old School RuneScape</a>
  <a class="nav-item nav-link" href="rs3.php">RuneScape 3</a>
  <a class="nav-item nav-link active" href="logged-in.php"><?=$nameTab?></a>
  <a class="nav-item nav-link" id="logout" style="display: <?$display?>;" href="logout.php">Logout</a>

    <?php
      if (isLoggedIn()) {
        $display = "block";
      }
    ?>
</nav>

<?php include "includes/form.php" ?>
<?php include "includes/footer.php" ?>