<?php
include "includes/mysql-connect.php";
include "includes/functions.php";
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if (!empty($_POST['confirm'])) {
		createUser($db);
	}

	else {
		login($db);
	}
}